<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomsMaster extends Model
{
    protected $table = 'room_type_master';
    protected $primaryKey = 'room_type_id';
}
