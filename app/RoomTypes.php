<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomTypes extends Model
{
    protected $table = 'hotel_room_types';
    protected $primaryKey = 'type_id';

    public function roomType()
    {
        return $this->belongsTo('App\RoomsMaster','room_type');
    }
}
