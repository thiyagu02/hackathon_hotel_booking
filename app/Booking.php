<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Hotels;
/**
* @BookingController
* @author Thiyagarajan
* @this class has to handle all the booking related methods
**/


class Booking extends Model
{
    protected $fillable = [ 'booking_id', 'hotel_id', 'hotel_room_type_id', 'booked_date', 'status', 'no_of_rooms', 'amount_paid', 'from_date','to_date','created_on','user_id'];
	protected $table = 'hotel_bookings';
	
	/**
	* To save the booking for a specific hotel
	* @params array
	* @return hotel booking status
	**/
	public static function saveBooking($request)
      {
		
		$hotel_booking = new Booking();
		$hotel_booking->hotel_id = $request->hotel_id;
		$hotel_booking->hotel_room_type_id = $request->hotel_room_type_id;
		$hotel_booking->from_date = date("Y-m-d H:i:s",strtotime($request->from_date));
		$hotel_booking->to_date = date("Y-m-d H:i:s",strtotime($request->to_date));
		$hotel_booking->created_on = date("Y-m-d H:i:s");
		$hotel_booking->status = 1;
		$hotel_booking->no_of_rooms = $request->no_of_rooms;
		$hotel_booking->amount_paid = $request->amount_paid;
		$hotel_booking->percentage_discount = $request->percentage_discount;
		$hotel_booking->user_id = $request->user_id;
		$hotel_response = $hotel_booking->save();
		
		// update availability to zero
		Hotels::updateAvailability($request->hotel_id);
		
		return $hotel_response
      }
}
