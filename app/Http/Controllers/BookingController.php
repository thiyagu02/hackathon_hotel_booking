<?php

namespace App\Http\Controllers;
use Validator;
use Illuminate\Http\Request;

class BookingController extends Controller
{
    //
	/**
	* To create hotel booking
	* @param array post values
	* @return mixed
	*/	
	public function store( $request)
	{
		// validator for mandatory options.
		$validator = Validator::make($request->all(), ['status' => 'required']);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
			// check the availability for the hotel
			$hotel_check = Hotels::getHotelAvailability($request->hotel_id); 
			if($hotel_check)
			{
				// success, proceed with hotel booking
				// save the booking for the respective hotel
				$booking_response = Booking::saveBooking($request);
				if($booking_response)
				{
					\Session::flash('flash_message', 'Booking is completed successfully!');
				}else{
					\Session::flash('flash_message', 'Booking failed, please try again');
				}
			}else{
				// failure class, redirect with error message
				\Session::flash('flash_message', 'Booking for this hotel is not available now!');
			}
		}
		return redirect('/');
	}
}
