<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hotels;

class SearchController extends FilecacheController
{
    /*
        Author: John 
        Description: To get hotels listing
    */

    public function getSearchResults(Request $request)
    {
        $objCache = new FilecacheController();
        $data['results'] = $objCache->getData($request->search);
     //   echo '<pre>';print_r($data);die;
        return view('hotels',$data);
    }

}
