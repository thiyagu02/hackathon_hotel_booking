<?php

namespace App\Http\Controllers;

use App\Adminview;
use Illuminate\Http\Request;
use DB;

class AdminviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $Bookinghistory=\DB::query("select hb.no_of_rooms,hb.amount_paid,hb.from_date,hb.to_date,hb.percentage_discount,hm.address,hm.city,hb.status as hotelstatus
        from hotel_bookings hb         
        left join hotel_master hm on hm.hotel_id=hb.hotel_id
        left join room_type_master rtp on rtp.room_type_id=hb.hotel_room_type_id");
        return view('admin.index', compact('Bookinghistory'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Adminview  $adminview
     * @return \Illuminate\Http\Response
     */
    public function show(Adminview $adminview)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Adminview  $adminview
     * @return \Illuminate\Http\Response
     */
    public function edit(Adminview $adminview)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Adminview  $adminview
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Adminview $adminview)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Adminview  $adminview
     * @return \Illuminate\Http\Response
     */
    public function destroy(Adminview $adminview)
    {
        //
    }
}
