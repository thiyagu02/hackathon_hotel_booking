<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Filecache;

/*
 * File Cache Class used to load the list of hotels with out any performance issue 
 * 
*/

class FilecacheController extends Controller
{
    protected $varFileName;
    protected $varExpires;
        public function __construct()
    {
        $this->varFileName = "loadHotelDetails.json";
        $this->varExpires = '500';
    }

    /* load the data from Database layer and load it in file cache to overcome file cache
     * check database for every 5mins and update in local cache
    */

    public function  setData(){
        // check the timing
	    if( !$this->varExpires) $this->varExpires = time() - 2*60*60;
		// check cache file exist
		$varCacheFile = public_path().'/'.$this->varFileName;
       
		
        $handle = fopen($varCacheFile, 'w') or die('Cannot open file:  '.$varCacheFile);
       

        $varFetchHotel = new Filecache();
        $arrHotelResult = $varFetchHotel->getData();
        $varHotelCount = 0;
        foreach($arrHotelResult as $varKey => $arrResult){
            $arrHotelResults[$varHotelCount]['hotel_id'] = $arrResult->hotel_id;
            $arrHotelResults[$varHotelCount]['varHotelName'] = $arrResult->hotel_name;
            $arrHotelResults[$varHotelCount]['varLocation'] = $arrResult->address;
            $arrHotelResults[$varHotelCount]['varCity'] = $arrResult->city;
            $arrHotelResults[$varHotelCount]['varZipCOde'] = $arrResult->zipcode;
            $arrHotelResults[$varHotelCount]['varStatus'] = $arrResult->hotel_status;
            $arrHotelResults[$varHotelCount]['varPrice'] =  $arrResult->roomTypes->amount;
            if(!empty($arrResult->roomTypes->roomType->room_name))
            $arrHotelResults[$varHotelCount]['varRoomType'] = $arrResult->roomTypes->roomType->room_name;
            else
            $arrHotelResults[$varHotelCount]['varRoomType'] = 'N/A';
            $varHotelCount++;
        }
        
        $arrHotelResults = json_encode($arrHotelResults);
        fwrite($handle, $arrHotelResults);               
		
		return 1;
    }

    /* Pass the search parameters to load the data based on the locations
     * search parameters date range, location, rooms, type of room
    */
    public function getData($searchParamaeter){ //array $searchParamaeter
            $handle = fopen($this->varFileName, 'r');
            $json_results = fread($handle,filesize($this->varFileName));
            $arrReadHotels = json_decode($json_results);
            $location = $roomType = '';
            $arrHotelSearchResult = array();
            if(isset($searchParamaeter['location']) && !empty($searchParamaeter['location'])){
           foreach ($arrReadHotels as $varHotelKey => $varHotelDetails) {
                     
                    if($searchParamaeter['location'] ==  $varHotelDetails->varCity){
                        $arrHotelSearchResult[] = $varHotelDetails;
                       // echo 'If';
                    }
                else{
                    $arrHotelSearchResult[] = $varHotelDetails;
                 //   echo 'Else ';
                }
            }
        }
          //  echo '<pre>';print_r($arrHotelSearchResult);die;
          if(count($arrHotelSearchResult) == 0) $arrHotelSearchResult = $arrReadHotels;

          if(isset($searchParamaeter['room_type']) && !empty($searchParamaeter['room_type'])){
            foreach ($arrHotelSearchResult as $varHotelKey => $varHotelDetails) {
               
                
                
                    if($searchParamaeter['room_type'] ==  $varHotelDetails->varRoomType){
                        $arrHotelSearchResult[] = $varHotelDetails;
                    }
                else{
                    $arrHotelSearchResult[] = $varHotelDetails;
                }              

                }
            }
        }
        $arrHotelSearchResult = (array) $arrHotelSearchResult;
            return $arrHotelSearchResult;
    }
}
