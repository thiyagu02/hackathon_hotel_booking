<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roomsearch extends Model
{
   protected $table='hotel_bookings';
   protected $primaryKey='booking_id';
   protected $fillable=['booking_id','hotel_id','hotel_room_type_id','booked_date','no_of_rooms','from_date','to_date'];
}
