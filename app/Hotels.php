<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use HotelBooking;

class Hotels extends Model
{
    protected $table = 'hotel_master';

    public function roomTypes()
    {
        return $this->belongsTo('App\RoomTypes','hotel_id')->with('roomType');
    }
	
	
	/**
	* To update the availability to zero when we done the booking for a hotel
	* @param $hotel_id
	* @return array
	**/
	public function updateAvailability($hotel_id)
	{
		$hotel = Hotels::find($hotel_id);
		$hotel->is_availability = 0;
		return $hotel->save();
	}
	
	/**
	* To check the availability of the hotel for booking
	* @param $hotel_id
	* @return is_availability
	**/
	  public function getHotelAvailability($hotel_id)
	  {
		  // check the master value of hotel availability
		  $hotel = Hotels::find($hotel_id);
		  $hotel_availability = $hotel->is_availability;
		  
			// check the dates available from the hotel booking
			$hotel_data = HotelBooking::where(['hotel_id' => $hotel_id])->where('from_date','>=',$from_date)->where('to_date','<=',$to_date)->get();
			if(count($hotel_data)>0 || empty($hotel_availability))
			{
				return false;
			}else{
				return true;
			}
	  }
}
