<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Hotels;

class Filecache extends Model
{
    //load data from database to manage in File Cache
    public function getData(){
        $varFetchHotel = Hotels::with('roomTypes')->where('hotel_status','1')->get();
        return $varFetchHotel;
    }
}
