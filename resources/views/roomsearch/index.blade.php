<!DOCTYPE html>
<html lang="en">
<head>
  <title>Hotel California Group</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Hotel California Group</h2>
  <h2>Please Search</h2>
  <div class="form-group">
  <label>From Date</label>
  <div class="input-group md-form form-sm form-1 pl-0">
  <div class="input-group-prepend">
    <span class="input-group-text purple lighten-3" id="basic-text1"><i class="fas fa-search text-white"
        aria-hidden="true"></i></span>
  </div>
  <input class="form-control my-0 py-1 datepicker" type="text" id="datepicker" placeholder="Search" aria-label="Search">
</div>
</div>


  <div class="form-group">
  <label>To Date</label>
  <div class="input-group md-form form-sm form-1 pl-0">
  <div class="input-group-prepend">
    <span class="input-group-text purple lighten-3" id="basic-text1"><i class="fas fa-search text-white"
        aria-hidden="true"></i></span>
  </div>
  <input class="form-control my-0 py-1" type="text" placeholder="Search" aria-label="Search">
</div>
</div>

<div class="form-group">
  <label>Location</label>
  <div class="input-group md-form form-sm form-1 pl-0">
  <div class="input-group-prepend">
    <span class="input-group-text purple lighten-3" id="basic-text1"><i class="fas fa-search text-white"
        aria-hidden="true"></i></span>
  </div>
   <input type="texxt" class="form-control " id="search['location']" placeholder="Location" name="location">
</div>
</div>


<div class="form-group">
  <label>Number of Rooms</label>
  <div class="input-group md-form form-sm form-1 pl-0">
  <div class="input-group-prepend">
    <span class="input-group-text purple lighten-3" id="basic-text1"><i class="fas fa-search text-white"
        aria-hidden="true"></i></span>
  </div>
   <select name="noofrooms" id="search['noofrooms']">
	<?php for($i=1;$i<10;$i++){?>
		<option value="<?php echo $i;?>"><?php echo $i;?></option>
	<?php } ?>
   </select>
</div>
</div>



<div class="form-group">
  <label>Room Type</label>
  <div class="input-group md-form form-sm form-1 pl-0">
  <div class="input-group-prepend">
    <span class="input-group-text purple lighten-3" id="basic-text1"><i class="fas fa-search text-white"
        aria-hidden="true"></i></span>
  </div>
  <select name="room_type">
            <option>Please select</option>
            <option value="1">Single</option>
            <option value="2">Double</option>
            <option value="3">Delux</option>
            <option value="4">Family</option>
      </select>
</div>
</div>

<button type="button" id="search" class="btn btn-default">Search</button>  
  <script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
  </script>
</body>
</html>
