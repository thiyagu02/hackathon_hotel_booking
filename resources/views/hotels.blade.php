@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Hotel California Group Search Hotels</div>

                <div class="card-body">
                    <form action="{{ route('search') }}" aria-label="{{ __('Login') }}">
                        @csrf
					  <div class="form-group">
					  <label>From Date</label>
					  <div class="input-group md-form form-sm form-1 pl-0">
					  <div class="input-group-prepend">
						<span class="input-group-text purple lighten-3" id="basic-text1"><i class="fas fa-search text-white"
							aria-hidden="true"></i></span>
					  </div>
					  <input placeholder="02/11/2019" class="form-control my-0 py-1 datepicker" type="text" name="search[fromdate]" placeholder="Search" aria-label="Search">
					</div>
					</div>


					  <div class="form-group">
					  <label>To Date</label>
					  <div class="input-group md-form form-sm form-1 pl-0">
					  <div class="input-group-prepend">
						<span class="input-group-text purple lighten-3" id="basic-text1"><i class="fas fa-search text-white"
							aria-hidden="true"></i></span>
					  </div>
					  <input placeholder="04/11/2019" class="form-control my-0 py-1" type="text" name="search[todate]" placeholder="Search" aria-label="Search">
					</div>
					</div>

					<div class="form-group">
					  <label>Location</label>
					  <div class="input-group md-form form-sm form-1 pl-0">
					  <div class="input-group-prepend">
						<span class="input-group-text purple lighten-3" id="basic-text1"><i class="fas fa-search text-white"
							aria-hidden="true"></i></span>
					  </div>
					   <input type="texxt" class="form-control " id="location" placeholder="Location" name="search[location]">
					</div>
					</div>


					<div class="form-group">
					  <label>Number of Rooms</label>
					  <div class="input-group md-form form-sm form-1 pl-0">
					  <div class="input-group-prepend">
						<span class="input-group-text purple lighten-3" id="basic-text1"><i class="fas fa-search text-white"
							aria-hidden="true"></i></span>
					  </div>
					   <select name="search[noofrooms]" id="noofrooms">
						<?php for($i=1;$i<10;$i++){?>
							<option value="<?php echo $i;?>"><?php echo $i;?></option>
						<?php } ?>
					   </select>
					</div>
					</div>



					<div class="form-group">
					  <label>Room Type</label>
					  <div class="input-group md-form form-sm form-1 pl-0">
					  <div class="input-group-prepend">
						<span class="input-group-text purple lighten-3" id="basic-text1"><i class="fas fa-search text-white"
							aria-hidden="true"></i></span>
					  </div>
					  <select name="search[room_type]">
								<option>Please select</option>
								<option value="Single">Single</option>
								<option value="Double">Double</option>
								<option value="Delux">Delux</option>
								<option value="Family">Family</option>
						  </select>
					</div>
					</div>
					<div class="form-group">
						<div class="input-group md-form form-sm form-1 pl-0">
							<input type="submit" value="Search" class="btn btn-success">
						</div>
					</div>
                    </form>
					<table class="table table-striped">
					<tr>
						<td><b>Hotel Name</b></td>
						<td><b>Address</b></td>
						<td><b>City</b></td>
						<td><b>Pincode</b></td>
						<td><b>Room Type</b></td>
						<td><b>Price</b></td>
						<td><b>Actions</b></td>
					</tr>
					@foreach($results as $key => $res)
						<tr>
							<td>{{ $res->varHotelName }}</td>
							<td>{{ $res->varLocation }}</td>
							<td>{{ $res->varCity }}</td>
							<td>{{ $res->varZipCOde }}</td>
							<td>{{ $res->varRoomType }}</td>
							<td>{{ $res->varPrice }}</td>
							@if(Auth::id())
							<td><a class="btn btn-primary" data-hotelid="{{ $res->hotel_id }}" data-price="{{ $res->varPrice }}">Book Now</a></td>
							@else 
							<td><a href="{{ route('login') }}" class="btn btn-primary">Login to Book Now</a></td>	
							@endif
						</tr>
					@endforeach
					</table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script type="text/javascript">
$('.datepicker').datepicker();
</script>