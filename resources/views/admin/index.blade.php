<link rel="stylesheet" href="{{{ URL::asset('css/bootstrap.min.css')}}}" />
  <link rel="stylesheet" href="{{{ URL::asset('font-awesome/4.2.0/css/font-awesome.min.css')}}}" />
  <link rel="stylesheet" href="{{{ URL::asset('fonts/fonts.googleapis.com.css')}}}" />
</head>
<body class="no-skin">
<div class="main-container" id="main-container">
  <div class="main-content"> 
     <table class="table table-striped table-bordered table-hover" id="bookinghistorytable">
                <thead>
                    <tr>
                        <th>Status</th>
                        <th>No of Rooms</th>
                        <th>Amount Paid</th>
                        <th>From Date</th>
                        <th>To date</th>
                        <th>Discount</th>
                        <th>Hotel Name</th>
                        <th>Address</th>
                        <th>city</th>
                        <!--<th>Room </th>-->
                    </tr>
                </thead>
                <tbody> <?php //print_r($Bookinghistory); exit;?>
                @foreach($Bookinghistory as $key => $val)
                    <tr>
                        <td>{{ $val->hotelstatus }}</td>
                        <td>{{ $val->no_of_rooms }}</td>
                        <td>{{ $val->amount_paid }}</td>
                        <td>{{ $val->from_date }}</td>
                        <td>{{ $val->to_date }}</td>
                        <td>{{ $val->percentage_discount }}</td>
                        <td>{{ $val->hotel_name }}</td>
                        <td>{{ $val->address }}</td>
                        <td>{{ $val->city }}</td>
                        <!--<td>{{ $val->room_name }}</td>-->
 
                        <!-- we will also add show, edit, and delete buttons -->
                       <!-- <td>-->
 
                            <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                          <!--  <a class="btn btn-small btn-success" href="{{ URL::to('employee/' . $emp->id) }}">Show</a>-->
 
                            <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
                           <!-- <a class="btn btn-small btn-info" href="{{ URL::to('employee/' . $emp->id . '/edit')}}">Edit</a>-->
 
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table> 
  </div>
 
  <script type="text/javascript" src="{{{ URL::asset('js/jquery.2.1.1.min.js')}}}"></script>
  <script src="{{{ URL::asset('js/bootstrap.min.js')}}}"></script>
 
  <script src="{{{ URL::asset('js/jquery.dataTables.min.js')}}}"></script>
  <script src="{{{ URL::asset('js/jquery.dataTables.bootstrap.min.js')}}}"></script>
</div>
</body>


1
2
3
4
5
<script>
 $(function() {
   $('#bookinghistorytable').DataTable();
 });
</script>