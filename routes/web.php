<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/setData', 'FilecacheController@setData');
Route::get('/SearchData', 'FilecacheController@getData');
Auth::routes('login','HomeController@');

Route::get('/home', 'HomeController@index')->name('home');
Route::any('/search', 'SearchController@getSearchResults')->name('search');

Route::get('test','BookingController@test');
Route::get('roomsearch','RoomsearchController@index');
Route::get('checkroom','RoomsearchController@index');
Route::get('adminviewdetails','AdminviewController@index');
